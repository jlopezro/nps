#+OPTIONS: toc:nil 
#+TITLE: Temario Módulo 2
#+OPTIONS: author:nil date:nil
#+LANGUAGE: es
* Objetivo 
Familiarizar al estudiante con los fundamentos del lenguaje de programación R y el ambiente de desarrollo Rstudio. El alumno aprenderá a utilizar los menús de ayuda que ofrece RStudio y la comunidad de usuarios . Demostrar algunas de las facilidades que ofrece R para manipular datos y para interactuar con fuentes de datos externas.
* Temas a Tratar
 - Conceptos básicos de R como lenguaje
 - Ambiente de trabajo
 - Git
 - Instalación de paquetes
 - Uso general de paquetes
 - Tipos de datos y variables
 - Iteraciones y Control de Flujo
 - Carga de datos
 - Conexión con diferentes fuentes de datos
 - Seleccionado y filtrado con dplyr
 - Join y merge en bases de datos
* Software a Instalar: 
 - Rstudio
 - R v4.0.5
* Formato de clase
La clase está estructurada de tal; manera que tengan suficiente tiempo de practicar. 
 - 1 Hr teoría
 - 2 Hrs Práctica
* Bibliografía
[[https://rstudio-education.github.io/hopr/][Hands-On Programing with R]]
